import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { HealthService } from '../shared/services/health.service';
import { User } from '../shared/models/user';

@Component({
    selector: 'health-doctor',
    templateUrl: 'doctor.component.html',
    styleUrls: ['doctor.component.scss'],
})

export class DoctorComponent implements OnInit {
    doctor: User;
    constructor(
        private activatedRoute: ActivatedRoute,
        private healthService: HealthService,
    ) {
        this.activatedRoute.params.subscribe((params: Params) => {
            this.healthService.getUsersById(params['id']).subscribe(res => {
                if (res.success) {
                    this.doctor = res.data;
                } else {
                    throw new Error(JSON.stringify(res.error));
                }
            });
        });
    }

    ngOnInit() { }
}
