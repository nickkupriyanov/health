import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler, LOCALE_ID } from '@angular/core';
import { Http, RequestOptions, HttpModule } from '@angular/http';
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { AppComponent } from './app.component';
import { HeaderModule } from './header/header.module';
import { SidenavModule } from './sidenav/sidenav.module';
import { FooterModule } from './footer/footer.module';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.component';
import { AuthGuardService } from './shared/services/auth-guard.service';
import { AuthService } from './shared/services/auth.service';
import { RoleGuardService } from './shared/services/role-guard.service';
import { ProfileGuardService } from './shared/services/profile-guard.service';

export function authHttpServiceFactory(
    http: Http,
    options: RequestOptions
) {
    return new AuthHttp(
        new AuthConfig({ noTokenScheme: true, noJwtError: true }),
        http,
        options
    );
}

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        HttpModule,
        BrowserModule,
        HeaderModule,
        SidenavModule,
        FooterModule,
        AppRoutingModule,
    ],
    providers: [
        AuthHttp,
        AuthService,
        AuthGuardService,
        RoleGuardService,
        ProfileGuardService,
        {
            provide: AuthHttp,
            useFactory: authHttpServiceFactory,
            deps: [Http, RequestOptions]
        },
        {
            provide: LOCALE_ID,
            useValue: 'ru-RU'
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
