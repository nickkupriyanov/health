import {
    Component,
    OnInit,
    ViewChild,
    ElementRef
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MdSnackBar } from '@angular/material';
import { ValidationService } from '../shared/services/validation.service';
import { HealthService } from '../shared/services/health.service';
import { User } from '../shared/models/user';
import { AuthService } from '../shared/services/auth.service';

@Component({
    selector: 'health-admin',
    templateUrl: 'admin.component.html',
    styleUrls: ['admin.component.scss'],
})

export class AdminComponent implements OnInit {
    @ViewChild('image') image: ElementRef;
    form: FormGroup;
    user = new User();
    src: string;
    file: File;
    workTime: { start: string, end: string } = { start: '', end: '' };

    constructor(
        private formBuilder: FormBuilder,
        private healthService: HealthService,
        private authService: AuthService,
        private mdSnackBar: MdSnackBar,
    ) {
        this.buildForm(this.user);
        this.healthService.getWorkTime().subscribe(res => {
            if (res.success) {
                this.workTime = res.data.time;
            } else {
                throw new Error(JSON.stringify(res.error));
            }
        });
    }

    ngOnInit() { }

    buildForm({ firstName = '', lastName = '', photo = '', type = '', about = '', password = '' }): void {
        this.form = this.formBuilder.group({
            'firstName': [firstName, [ValidationService.firstNameValidator, ValidationService.emptyFieldValidator]],
            'lastName': [lastName, [ValidationService.lastNameValidator, ValidationService.emptyFieldValidator]],
            'photo': [photo],
            'type': [type, ValidationService.emptyFieldValidator],
            'about': [about, ValidationService.emptyFieldValidator],
            'password': [password, [ValidationService.emptyFieldValidator, ValidationService.passwordValidator]]
        });
    }

    choosePhoto(): void {
        this.image.nativeElement.value = null;
        this.image.nativeElement.click();
    }

    onFileChange(event) {
        this.file = event.target.files[0];
        const reader = new FileReader();
        reader.onload = (e: any) => {
            this.src = e.target.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    }

    addNewDoctor() {
        console.log(this.form.value);
        // this.authService.signUp(this.user).subscribe(res => {
        //     if (res.success) {
        //         this.user = new User;
        //         this.buildForm(this.user);
        //         this.mdSnackBar.open('Новый доктор добавлен', '', {
        //             duration: 1500,
        //         });
        //     } else {
        //         throw new Error(JSON.stringify(res.error));
        //     }
        // });
    }

    clear() {
        this.buildForm(new User());
    }

    saveTime() {
        this.healthService.editWorkTime(this.workTime).subscribe(res => {
            if (res.success) {
                this.mdSnackBar.open('Время обновлено', '', {
                    duration: 1500,
                });
            } else {
                throw new Error(JSON.stringify(res.error));
            }
        });
    }
}
