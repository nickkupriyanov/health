import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FacebookService, InitParams, LoginResponse } from 'ngx-facebook';
import { AuthService as GoogleService } from 'angular2-social-login';
import { ValidationService } from '../shared/services/validation.service';
import { AuthService } from '../shared/services/auth.service';
import { User } from '../shared/models/user';
import { environment } from '../../environments/environment';
import { HealthService } from '../shared/services/health.service';
declare const VK;

@Component({
    selector: 'health-auth',
    templateUrl: 'auth.component.html',
    styleUrls: ['auth.component.scss'],
    providers: [
        ValidationService,
        AuthService,
        GoogleService,
        FacebookService
    ],
})

export class AuthComponent implements OnInit {
    state = 'signin';
    signInForm: FormGroup;
    signUpForm: FormGroup;
    forgotForm: FormGroup;
    currentSocialId: any;
    id: string;
    info: string;
    isSocialSignUp = false;
    user = new User();

    constructor(
        private formBuilder: FormBuilder,
        private validationService: ValidationService,
        private googleService: GoogleService,
        private facebookService: FacebookService,
        private authService: AuthService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private healthService: HealthService,
    ) {
        this.buildSignInForm(this.user);
        this.activatedRoute.params.subscribe((params: Params) => {
            if (params['id']) {
                this.healthService.getUsersById(params['id']).subscribe(
                    res => {
                        if (res.success) {
                            this.user.doctors = [res.data];
                        } else {
                            throw new Error(JSON.stringify(res.error));
                        }
                    },
                    err => {
                        throw new Error(JSON.stringify(err));
                    },
                    () => {
                        this.state = 'signup';
                        this.buildSignUpForm(this.user);
                    }
                );
            }
        });
        const initParams: InitParams = {
            appId: environment.facebookClientId,
            xfbml: true,
            version: environment.facebookApiVersion,
        };

        facebookService.init(initParams);
        VK.init({
            apiId: environment.vkontakteClientId,
        });
    }

    ngOnInit() { }

    changeTemplate(state: string, isSocial?: boolean) {
        if (!isSocial) {
            this.isSocialSignUp = false;
        }
        switch (state) {
            case 'forgot':
                this.info = 'Введите адрес электронной почты и мы вышлем на него инструкции по восстановлению пароля';
                this.buildForgotForm(this.user);
                this.state = 'forgot';
                break;
            case 'signin':
                isSocial ? this.info = 'Войдите чтобы привязать' : delete this.info;
                this.buildSignInForm(this.user);
                this.state = 'signin';
                break;
            case 'signup':
                this.buildSignUpForm(this.user);
                this.info = 'Создайте профиль чтобы продолжить';
                this.state = 'signup';
                break;
        }
    }

    buildSignInForm({ email = '', gId = '', vId = '', fId = '' }) {
        this.signInForm = this.formBuilder.group({
            'email': [
                email,
                [ValidationService.emailValidator, ValidationService.emptyFieldValidator],
                this.validationService.notExistEmail.bind(this.validationService)
            ],
            'password': ['', [ValidationService.passwordValidator, ValidationService.emptyFieldValidator]],
            'gId': [gId],
            'fId': [fId],
            'vId': [vId],
        });
    }

    buildSignUpForm({ email = '', firstName = '', lastName = '', gId = '', vId = '', fId = '', doctors = [] }) {
        this.signUpForm = this.formBuilder.group({
            'email': [
                email,
                [ValidationService.emailValidator, ValidationService.emptyFieldValidator],
                this.validationService.existEmail.bind(this.validationService)
            ],
            'firstName': [firstName, [ValidationService.firstNameValidator, ValidationService.emptyFieldValidator]],
            'lastName': [lastName, [ValidationService.lastNameValidator, ValidationService.emptyFieldValidator]],
            'password': ['', [ValidationService.passwordValidator, ValidationService.emptyFieldValidator]],
            'passwordRepeat': ['', [ValidationService.emptyFieldValidator]],
            'gId': [gId],
            'fId': [fId],
            'vId': [vId],
            'doctors': [doctors],
        }, {
                validator: ValidationService.confirmPasswordValidator,
            });
    }

    buildForgotForm({ email = '' }) {
        this.forgotForm = this.formBuilder.group({
            'email': [
                email,
                [ValidationService.emailValidator, ValidationService.emptyFieldValidator],
                this.validationService.notExistEmail.bind(this.validationService)
            ],
        });
    }

    choiceSocial(social: string) {
        this.state === 'signin' ? this.buildSignInForm(this.user) : this.buildSignUpForm(this.user);
        switch (social) {
            case 'facebook':
                this.signInViaFacebook();
                break;
            case 'google':
                this.signInViaGoogle();
                break;
            case 'vk':
                this.signInViaVkontakte();
                break;
        }
    }

    signInViaFacebook() {
        this.facebookService.login(
            { scope: 'email, public_profile' }
        )
            .then((res: LoginResponse) => {
                this.authService.getFbProfile(res.authResponse.accessToken, res.authResponse.userID)
                    .subscribe(r => {
                        const user: User = {
                            fId: r.id,
                            email: r.email,
                            lastName: r.last_name,
                            firstName: r.first_name,
                        };
                        this.signInViaSocial(user);
                    });
            })
            .catch(err => {
                throw new Error(JSON.stringify(err));
            });
    }

    signInViaGoogle() {
        this.googleService.login('google').subscribe(
            (res: any) => {
                const user: User = {
                    gId: res.uid,
                    email: res.email,
                    firstName: res.name.split(' ')[0],
                    lastName: res.name.split(' ')[1],
                };
                this.signInViaSocial(user);
            },
            err => {
                throw new Error(JSON.stringify(err));
            }
        );
    }

    signInViaVkontakte() {
        VK.Auth.login((res) => {
            const user: User = {
                vId: res.session.user.id,
                firstName: res.session.user.first_name,
                lastName: res.session.user.last_name,
            };
            this.signInViaSocial(user);
        },
            err => {
                throw new Error(JSON.stringify(err));
            }
        );
    }

    signInViaSocial(user: User) {
        ['fId', 'gId', 'vId'].forEach(k => {
            if (user[k]) {
                this.currentSocialId = { [k]: user[k] };
            }
        });
        this.authService.signInViaSocial(user).subscribe(response => {
            if (response.success) {
                switch (response.data.flag) {
                    case 'token':
                        this.saveTokenAndRedirect(response.data.token);
                        break;
                    case 'emailExist':
                        this.state = 'signin';
                        this.buildSignInForm(user);
                        this.info = `Что бы связать социальную сеть с Вашим аккаунтом заполните поля`;
                        break;
                    case 'newUser':
                        this.state = 'signup';
                        this.buildSignUpForm(user);
                        this.info = 'Заполните недостающую информацию';
                        this.isSocialSignUp = true;
                        break;
                }
            } else {
                throw new Error(JSON.stringify(response.error));
            }
        });
    }

    saveTokenAndRedirect(token: string) {
        localStorage.setItem('token', token);
        this.authService.getUser().subscribe(res => {
            if (res.success) {
                const user: User = res.data;
                const requiredField = [
                    user.middleName,
                    user.gender,
                    user.phone,
                    user.photo,
                ];
                if (requiredField.includes('') && user.role === 1) {
                    this.router.navigate(['/intermediate', this.id ? this.id : '']);
                } else {
                    this.router.navigate(['/calendar']);
                }
            } else {
                throw new Error(JSON.stringify(res.error));
            }
        });
    }

    signIn() {
        if (this.currentSocialId) {
            Object.assign(this.signInForm.value, this.currentSocialId);
        }
        this.authService.signIn(this.signInForm.value).subscribe(res => {
            if (res.success) {
                this.saveTokenAndRedirect(res.data);
            } else {
                this.info = 'Неверный пароль';
                throw new Error(JSON.stringify(res.error));
            }
        });
    }

    signUp() {
        this.signUpForm.value.role = 1;
        this.authService.signUp(this.signUpForm.value).subscribe(res => {
            if (res.success) {
                this.saveTokenAndRedirect(res.data);
            } else {
                throw new Error(JSON.stringify(res.error));
            }

        });
    }

    forgot() {
        console.log('forgot');
    }
}
