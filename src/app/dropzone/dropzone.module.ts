import { NgModule } from '@angular/core';
import { DropzoneComponent } from './dropzone.component';
import { DropzoneDirective } from './dropzone.directive';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    imports: [
        SharedModule,
    ],
    exports: [
        DropzoneDirective,
        DropzoneComponent,
    ],
    declarations: [
        DropzoneComponent,
        DropzoneDirective,
    ],
    providers: [],
})
export class DropzoneModule { }
