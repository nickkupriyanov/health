import {
    Component,
    OnInit,
    ElementRef,
    ViewChild,
    EventEmitter,
    Output
} from '@angular/core';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { DropzoneService } from '../shared/services/dropzone.service';

@Component({
    selector: 'health-dropzone',
    templateUrl: 'dropzone.component.html',
    styleUrls: ['dropzone.component.scss'],
})

export class DropzoneComponent implements OnInit {
    displayedColumns = ['name', 'size', 'action'];
    dataSource: CustomDataSource;
    @ViewChild('input') input: ElementRef;
    @Output() uploadEvent = new EventEmitter();
    files = [];

    constructor(
        private dropzoneService: DropzoneService,
    ) {
        this.dropzoneService.observable$.subscribe(res => {
            this.files = [];
            this.dataSource.subject.next(this.files);
        });
    }

    ngOnInit() {
        this.dataSource = new CustomDataSource();
    }

    onDropzoneClick() {
        this.input.nativeElement.value = null;
        this.input.nativeElement.click();
    }

    onFilesDropped(files) {
        for (const file of files) {
            this.files.push(file);
        }
        this.dataSource.subject.next(this.files);
    }

    upload() {
        this.uploadEvent.emit(this.files);
    }

    clear() {
        this.files = [];
        this.dataSource.subject.next(this.files);
    }

    delete(file) {
        this.files.splice(this.files.indexOf(file), 1);
        this.dataSource.subject.next(this.files);
    }

    formatBytes(bytes, decimals?) {
        if (bytes === 0) {
            return '0 Bytes';
        }
        const k = 1000;
        const dm = decimals || 2;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        const i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }
}

export class CustomDataSource extends DataSource<any> {
    subject = new Subject<any>();
    obs$ = this.subject.asObservable();
    connect(collectionViewer): Observable<any[]> {
        return this.obs$;
    }
    disconnect(collectionViewer): void {
    }
}
