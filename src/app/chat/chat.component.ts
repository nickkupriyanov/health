import { Component, OnInit } from '@angular/core';
import { User } from '../shared/models/user';
import { AuthService } from '../shared/services/auth.service';
import { HealthService } from '../shared/services/health.service';
import { SocketService } from '../shared/services/socket.service';
import { MessagesService } from '../shared/services/messages.service';

@Component({
    selector: 'health-chat',
    templateUrl: 'chat.component.html',
    styleUrls: ['chat.component.scss'],
})

export class ChatComponent implements OnInit {
    user: User;
    interlocutors: User[] = [];
    selectedInterlocutor: User;
    messageText: string;
    room: string;
    messages: any[] = [];

    constructor(
        private authService: AuthService,
        private healthService: HealthService,
        private socketService: SocketService,
        private messagesService: MessagesService,
    ) {
        this.messagesService.observable$.subscribe(res => {
            this.messages.push(res);
        });
        this.authService.getUser().subscribe(res => {
            if (res.success) {
                this.user = res.data;
                this.getUsersByRole(this.user);
            } else {
                throw new Error(JSON.stringify(res.error));
            }
        });
    }

    ngOnInit() { }

    get isAdmin() {
        return this.user.role === 0 ? true : false;
    }

    get getCurrentChatMessages() {
        if (this.selectedInterlocutor) {
            return this.messages
                .filter(m => m.fromId === this.selectedInterlocutor.id || m.toId === this.selectedInterlocutor.id);
        }
    }

    isIncoming(message) {
        return message.fromId === this.user.id ? false : true;
    }

    getUsersByRole(user: User) {
        this.healthService.getUsersByRole(user.role).subscribe(res => {
            if (res.success) {
                this.interlocutors = res.data;
                this.selectedInterlocutor = this.interlocutors[0];
            } else {
                throw new Error(JSON.stringify(res.error));
            }
        });
    }

    chooseInterlocutor(interlocutor: User) {
        this.selectedInterlocutor = interlocutor;
    }

    sendMessage() {
        if (this.isAdmin) {
            this.room = `${this.selectedInterlocutor.id}-${this.user.id}`;
        } else {
            this.room = `${this.user.id}-${this.selectedInterlocutor.id}`;
        }
        const data = {
            date: new Date(),
            fromId: this.user.id,
            toId: this.selectedInterlocutor.id,
            room: this.room,
            message: this.messageText,
        };
        if (this.messageText === '' || !this.messageText) {
            return;
        }
        this.socketService.sendMessage(data);
        this.messageText = '';
    }

    onInputPressKey(event) {
        if (event.keyCode === 13) {
            this.sendMessage();
        }
    }
}
