import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatComponent } from './chat.component';
import { AuthGuardService } from '../shared/services/auth-guard.service';
import { ProfileGuardService } from '../shared/services/profile-guard.service';

const routes: Routes = [
    {
        path: 'chat',
        component: ChatComponent,
        canActivate: [AuthGuardService, ProfileGuardService],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ChatRoutingModule { }

export const routedComponents = [ChatComponent];
