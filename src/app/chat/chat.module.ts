import { NgModule } from '@angular/core';
import { ChatComponent } from './chat.component';
import { ChatRoutingModule } from './chat-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    imports: [
        ChatRoutingModule,
        SharedModule,
    ],
    exports: [],
    declarations: [ChatComponent],
    providers: [],
})

export class ChatModule { }
