import { Component, OnInit } from '@angular/core';
import { ToggleSidenavService } from '../shared/services/toggle-sidenav.service.component';
import { AuthService } from '../shared/services/auth.service';

@Component({
    selector: 'health-header',
    templateUrl: 'header.component.html',
    styleUrls: ['header.component.scss']
})

export class HeaderComponent implements OnInit {
    constructor(
        private toggleSidenavService: ToggleSidenavService,
        private authService: AuthService,
    ) { }

    ngOnInit() { }

    toggleSidenav() {
        this.toggleSidenavService.change();
    }

    toggleNotifications() {
        this.toggleSidenavService.changeN();
    }

    signOut() {
        this.authService.signOut();
    }
}
