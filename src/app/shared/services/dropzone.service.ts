import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class DropzoneService {
    // clear dropzone after upload files
    private source = new Subject<any>();
    // private sourceN = new Subject<any>();

    public observable$ = this.source.asObservable();
    // public observableN$ = this.sourceN.asObservable();

    public change(state?: any) {
        this.source.next(state);
    }
    // public changeN(state?: any) {
    //     this.sourceN.next(state);
    // }
}
