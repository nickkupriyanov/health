import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class MessagesService {
    // pass message from app to chat
    private source = new Subject<any>();

    public observable$ = this.source.asObservable();

    public change(state?: any) {
        this.source.next(state);
    }
}
