import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class RoleGuardService implements CanActivate {
    constructor(
        private authService: AuthService,
        private router: Router,
    ) { }

    canActivate(activatedRouteSnapshot: ActivatedRouteSnapshot) {
        return this.authService.getUser().map(res => {
            if (res.success) {
                if (res.data.role === 0) {
                    return true;
                } else {
                    this.router.navigate(['calendar']);
                    return false;
                }
            }
        });
    }
}
