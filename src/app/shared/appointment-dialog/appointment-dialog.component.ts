import {
    Component,
    OnInit,
    Input
} from '@angular/core';
import { MdDialogRef } from '@angular/material';
import { User } from '../models/user';

@Component({
    selector: 'health-appointment-dialog',
    templateUrl: 'appointment-dialog.component.html',
    styleUrls: ['appointment-dialog.component.scss']
})

export class AppointmentDialogComponent implements OnInit {
    @Input() date: Date;
    @Input() doctor: User;
    @Input() times;
    @Input() events;
    @Input() role;
    addition = false;
    selectedTime;

    constructor(
        public dialogRef: MdDialogRef<AppointmentDialogComponent>
    ) { }

    ngOnInit() {
    }

    selectTime(time) {
        this.selectedTime = time;
        this.addition = true;
    }

    add() {
        this.dialogRef.close({ action: 'add', value: this.selectedTime.time });
    }
}
