import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
    MdToolbarModule,
    MdButtonModule,
    MdIconModule,
    MdSidenavModule,
    MdCardModule,
    MdInputModule,
    MdCheckboxModule,
    MdSelectModule,
    MdListModule,
    MdMenuModule,
    MdTabsModule,
    MdTableModule,
    MdRadioModule,
    MdSnackBarModule,
    MdDialogModule,
    MdExpansionModule,
    MdTooltipModule,
} from '@angular/material';
import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome';
import { ErrorMessageComponent } from './error-message/error-message.component';
import { CdkTableModule } from '@angular/cdk';

@NgModule({
    imports: [
        MdToolbarModule,
        MdButtonModule,
        MdIconModule,
        MdSidenavModule,
        MdCardModule,
        MdInputModule,
        MdCheckboxModule,
        MdSelectModule,
        MdListModule,
        MdMenuModule,
        MdTabsModule,
        MdTableModule,
        MdRadioModule,
        MdSnackBarModule,
        MdDialogModule,
        MdExpansionModule,
        MdTooltipModule,
        CdkTableModule,
        CommonModule,
        BrowserAnimationsModule,
        Angular2FontawesomeModule,
        ReactiveFormsModule,
        FormsModule,
    ],
    exports: [
        MdToolbarModule,
        MdButtonModule,
        MdIconModule,
        MdSidenavModule,
        MdCardModule,
        MdInputModule,
        MdCheckboxModule,
        MdSelectModule,
        MdListModule,
        MdMenuModule,
        MdTabsModule,
        MdTableModule,
        MdRadioModule,
        MdSnackBarModule,
        MdDialogModule,
        MdExpansionModule,
        MdTooltipModule,
        CdkTableModule,
        CommonModule,
        BrowserAnimationsModule,
        Angular2FontawesomeModule,
        ReactiveFormsModule,
        FormsModule,
        ErrorMessageComponent,
    ],
    declarations: [ErrorMessageComponent],
    providers: [],
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: []
        };
    }
}
