import { Component, OnInit } from '@angular/core';
import { MdDialogRef } from '@angular/material';

@Component({
    selector: 'health-delte-dialog',
    templateUrl: 'delete-dialog.component.html'
})

export class DeleteDialogComponent implements OnInit {
    name: string;
    constructor(
        public dialogRef: MdDialogRef<DeleteDialogComponent>
    ) { }

    ngOnInit() { }
}
