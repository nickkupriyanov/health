import { Component, OnInit } from '@angular/core';
import { MdDialogRef } from '@angular/material';

@Component({
    selector: 'health-rename-dialog',
    templateUrl: 'rename-dialog.component.html',
})

export class RenameDialogComponent implements OnInit {
    name: string;
    newName: string;

    constructor(
        public dialogRef: MdDialogRef<RenameDialogComponent>
    ) { }

    ngOnInit() { }
}
