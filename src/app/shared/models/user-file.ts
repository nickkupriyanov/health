export class UserFile {
    id: string;
    name: string;
    originalName: string;
    path: string;
}

