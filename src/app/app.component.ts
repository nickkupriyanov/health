import { Component, OnInit } from '@angular/core';
import {
    Router,
    ActivatedRoute,
    RoutesRecognized,
    NavigationEnd
} from '@angular/router';
import { ToggleSidenavService } from './shared/services/toggle-sidenav.service.component';
import { HealthService } from './shared/services/health.service';
import { AuthService } from './shared/services/auth.service';
import { SocketService } from './shared/services/socket.service';
import { MessagesService } from './shared/services/messages.service';
import { User } from './shared/models/user';

@Component({
    selector: 'health-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    providers: [
        ToggleSidenavService,
        HealthService,
        AuthService,
        SocketService,
        MessagesService,
    ],
})
export class AppComponent implements OnInit {
    show = true;
    user: User;
    messages: any[] = [];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private authService: AuthService,
        private socketService: SocketService,
        private healthService: HealthService,
        private messagesService: MessagesService,
    ) {
        if (this.authService.isAuth()) {
            this.socketService.connection().subscribe();
            this.authService.getUser().subscribe(res => {
                if (res.success) {
                    this.user = res.data;
                    if (this.user.role === 0) {
                        this.enterAdminToRoom();
                    } else {
                        this.enterUserToRoom();
                    }
                }
            });
        }
        router.events.subscribe((res: NavigationEnd) => {
            if (res instanceof NavigationEnd) {
                if (res.url === '/' ||
                    res.url.includes('home') ||
                    res.url.includes('intermediate') ||
                    res.url.includes('doctor') ||
                    res.url.includes('auth')
                ) {
                    this.show = false;
                } else {
                    this.show = true;
                }
            }
        });
    }

    ngOnInit(): void {
        this.getMessages();
    }

    enterAdminToRoom() {
        this.healthService.getUsers().subscribe(r => {
            if (r.success) {
                for (const u of r.data) {
                    if (u.id !== this.user.id) {
                        this.socketService.enterRoom({
                            user: this.user.email,
                            room: `${u.id}-${this.user.id}`
                        });
                    }
                }
            }
        });
    }

    enterUserToRoom() {
        this.healthService.getAdmins().subscribe(r => {
            if (r.success) {
                for (const u of r.data) {
                    this.socketService.enterRoom({
                        user: this.user.email,
                        room: `${this.user.id}-${u.id}`
                    });
                }
            }
        });
    }

    getMessages() {
        this.socketService.getMessages().subscribe((message: any) => {
            console.log(message);
            this.messages.push(message);
            this.messagesService.change(message);
        });
    }
}
