import { NgModule } from '@angular/core';
import { SidenavComponent } from './sidenav.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { HomeModule } from '../home/home.module';
import { CalendarModule } from '../calendar/calendar.module';
import { AuthModule } from '../auth/auth.module';
import { IntermediateModule } from '../intermediate/intermediate.module';
import { FilesModule } from '../files/files.module';
import { ProfileModule } from '../profile/profile.module';
import { ChatModule } from '../chat/chat.module';
import { AdminModule } from '../admin/admin.module';
import { DoctorModule } from '../doctor/doctor.module';

@NgModule({
    imports: [
        SharedModule,
        RouterModule,
        HomeModule,
        CalendarModule,
        FilesModule,
        ProfileModule,
        ChatModule,
        AuthModule,
        AdminModule,
        DoctorModule,
        IntermediateModule,
    ],
    exports: [SidenavComponent],
    declarations: [SidenavComponent],
    providers: [],
})
export class SidenavModule { }
