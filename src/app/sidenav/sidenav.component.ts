import {
    Component,
    OnInit,
    ElementRef,
    ViewChild
} from '@angular/core';
import { MdSidenav } from '@angular/material';
import { ToggleSidenavService } from '../shared/services/toggle-sidenav.service.component';
import { AuthService } from '../shared/services/auth.service';
import { User } from '../shared/models/user';

@Component({
    selector: 'health-sidenav',
    templateUrl: 'sidenav.component.html',
    styleUrls: ['sidenav.component.scss']
})

export class SidenavComponent implements OnInit {
    @ViewChild('sidenav') sidenav: MdSidenav;
    @ViewChild('notifications') notifications: MdSidenav;
    user = new User;

    constructor(
        private toggleSidenavService: ToggleSidenavService,
        private authService: AuthService,
    ) {
        if (this.authService.isAuth()) {
            this.authService.getUser().subscribe(res => {
                if (res.success) {
                    this.user = res.data;
                } else {
                    throw new Error(JSON.stringify(res.error));
                }
            });
        }
        this.toggleSidenavService.observable$.subscribe(res => {
            this.sidenav.toggle();
        });
        this.toggleSidenavService.observableN$.subscribe(res => {
            this.notifications.toggle();
        });
    }

    ngOnInit() { }

    get isAdmin() {
        return this.user.role === 0 ? true : false;
    }
}
