import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HealthService } from '../shared/services/health.service';
import { User } from '../shared/models/user';
import { AuthService } from '../shared/services/auth.service';

@Component({
    selector: 'health-home',
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.scss'],
})

export class HomeComponent implements OnInit {
    doctors: User[] = [];
    constructor(
        private healthService: HealthService,
        private authService: AuthService,
        private router: Router,
    ) {
        if (this.authService.isAuth()) {
            this.router.navigate(['calendar']);
        }
        healthService.getAdmins().subscribe(res => {
            if (res.success) {
                this.doctors = res.data;
            }
        });
    }

    ngOnInit() { }
}
