import {
    Component,
    OnInit,
    ElementRef,
    ViewChild
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MdSnackBar } from '@angular/material';
import { User } from '../shared/models/user';
import { AuthService } from '../shared/services/auth.service';
import { ValidationService } from '../shared/services/validation.service';
import { HealthService } from '../shared/services/health.service';
import { Router, NavigationEnd, ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'health-profile',
    templateUrl: 'profile.component.html',
    styleUrls: ['profile.component.scss'],
    providers: [ValidationService]
})

export class ProfileComponent implements OnInit {
    @ViewChild('image') image: ElementRef;
    user = new User();
    form: FormGroup;
    allDoctors: User[];
    selectedDoctors: User[] = [];
    selectedIndex;

    constructor(
        private authService: AuthService,
        private formBuilder: FormBuilder,
        private healthService: HealthService,
        public snackBar: MdSnackBar,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private validationService: ValidationService,
    ) {
        this.buildForm(this.user);
        this.activatedRoute.params.subscribe((params: Params) => {
            this.selectedIndex = params['id'];
        });
        this.authService.getUser().subscribe(res => {
            if (res.success) {
                this.user = res.data;
                this.healthService.getAdmins().subscribe(r => {
                    if (r.success) {
                        this.allDoctors = r.data;
                        if (this.user.doctors) {
                            this.user.doctors = this.allDoctors.filter(f => this.user.doctors.map(d => d.id).includes(f.id));
                        }
                        this.buildForm(this.user);
                    } else {
                        throw new Error(JSON.stringify(r.error));
                    }
                });
            } else {
                throw new Error(JSON.stringify(res.error));
            }
        });
    }

    ngOnInit() { }

    // tslint:disable-next-line:max-line-length
    buildForm({ firstName = '', lastName = '', middleName = '', gender = '', birth = '', phone = '', location = '', photo = '', doctors = [] }): void {
        this.form = this.formBuilder.group({
            'firstName': [firstName, [ValidationService.firstNameValidator, ValidationService.emptyFieldValidator]],
            'lastName': [lastName, [ValidationService.lastNameValidator, ValidationService.emptyFieldValidator]],
            'middleName': [middleName, [ValidationService.middleNameValidator, ValidationService.emptyFieldValidator]],
            'gender': [gender],
            'birth': [birth, [ValidationService.birthValidator, ValidationService.emptyFieldValidator]],
            'phone': [phone, [ValidationService.phoneValidator, ValidationService.emptyFieldValidator]],
            'location': [location, [ValidationService.emptyFieldValidator]],
            'photo': [photo],
            'doctors': [doctors, [], this.validationService.doctorLengthValidator.bind(this.validationService)]
        });
    }

    save() {
        this.healthService.editProfile(this.form.value).subscribe(res => {
            if (res.success) {
                this.user = this.form.value;
                if (this.selectedIndex) {
                    this.router.navigate(['calendar']);
                }
                this.snackBar.open('Профиль обновлен', '', {
                    duration: 1000,
                });
            } else {
                throw new Error(JSON.stringify(res.error));
            }
        });
    }

    choosePhoto(): void {
        this.image.nativeElement.value = null;
        this.image.nativeElement.click();
    }

    onFileChange(file: File) {
        this.healthService.uploadProfilePhoto(file).subscribe(res => {
            if (res.success) {
                this.user.photo = res.data;
                this.healthService.editProfile(this.user).subscribe(r => {
                    if (r.success) {
                        this.user.photo = res.data;
                        this.form.value.photo = res.data;
                        this.snackBar.open('Фото обновлено', '', {
                            duration: 1000,
                        });
                    } else {
                        throw new Error(JSON.stringify(res.error));
                    }
                });
            } else {
                throw new Error(JSON.stringify(res.error));
            }
        });
    }

    selectDoctor(doctor) {
        if (this.selectedDoctors.map(m => m.id).includes(doctor.id)) {
            this.selectedDoctors.splice(this.selectedDoctors.map(m => JSON.stringify(m)).indexOf(
                JSON.stringify(this.selectedDoctors.find(f => f.id === doctor.id))
            ), 1);
        } else {
            this.selectedDoctors.push(doctor);
        }
        this.form.value.doctors = this.selectedDoctors;
    }

    isSelected(doctor) {
        return this.selectedDoctors.map(m => m.id).includes(doctor.id);
    }
}
