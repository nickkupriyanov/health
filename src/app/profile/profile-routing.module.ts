import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile.component';
import { AuthGuardService } from '../shared/services/auth-guard.service';
import { ProfileGuardService } from '../shared/services/profile-guard.service';

const routes: Routes = [
    {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [AuthGuardService, ProfileGuardService]
    },
    {
        path: 'profile/:id',
        component: ProfileComponent,
        canActivate: [AuthGuardService, ProfileGuardService]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ProfileRoutingModule { }

export const routedComponents = [ProfileComponent];
