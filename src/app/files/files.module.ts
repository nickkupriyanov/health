import { NgModule } from '@angular/core';
import { FilesComponent } from './files.component';
import { FilesRoutingModule } from './files-routing.module';
import { SharedModule } from '../shared/shared.module';
import { DeleteDialogComponent } from '../shared/delete-dialog/delete-dialog.component';
import { RenameDialogComponent } from '../shared/rename-dialog/rename-dialog.component';
import { DropzoneModule } from '../dropzone/dropzone.module';

@NgModule({
    imports: [
        FilesRoutingModule,
        SharedModule,
        DropzoneModule,
    ],
    exports: [],
    declarations: [
        FilesComponent,
        DeleteDialogComponent,
        RenameDialogComponent,
    ],
    providers: [],
    entryComponents: [
        DeleteDialogComponent,
        RenameDialogComponent,
    ]
})
export class FilesModule { }
