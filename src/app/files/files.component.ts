import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MdDialog, MdDialogRef, MdSnackBar } from '@angular/material';
import { DataSource } from '@angular/cdk';
import { UserFile } from '../shared/models/user-file';
import { HealthService } from '../shared/services/health.service';
import { AuthService } from '../shared/services/auth.service';
import { DeleteDialogComponent } from '../shared/delete-dialog/delete-dialog.component';
import { RenameDialogComponent } from '../shared/rename-dialog/rename-dialog.component';
import { DropzoneService } from '../shared/services/dropzone.service';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'health-files',
    templateUrl: 'files.component.html',
    styleUrls: ['files.component.scss'],
    providers: [MdDialog, DropzoneService]
})

export class FilesComponent implements OnInit {
    files: UserFile[];
    selectedFile: UserFile;

    constructor(
        private healthService: HealthService,
        private authService: AuthService,
        private mdDialog: MdDialog,
        private mdSnackBar: MdSnackBar,
        private dropzoneService: DropzoneService,
    ) {
        this.authService.getUser().subscribe(res => {
            if (res.success) {
                this.files = res.data.files;
            } else {
                throw new Error(JSON.stringify(res.error));
            }
        });
    }

    ngOnInit() { }

    setIcon(file) {
        const extension = file.name.split('.').pop();
        const config = {
            'pdf': 'file-pdf-o',
            'docx': 'file-word-o',
            'doc': 'file-word-o',
            'png': 'file-image-o',
            'jpg': 'file-image-o',
        };
        return config[extension] ? config[extension] : 'file-o';
    }

    onFileClick(file) {
        this.selectedFile === file ? delete this.selectedFile : this.selectedFile = file;
        // this.selectedFile = file;
    }

    delete() {
        const deleteDialogRef: MdDialogRef<DeleteDialogComponent> = this.mdDialog.open(DeleteDialogComponent);
        deleteDialogRef.componentInstance.name = this.selectedFile.originalName;
        deleteDialogRef.afterClosed().subscribe(res => {
            if (res) {
                this.healthService.deleteFile(this.selectedFile.id).subscribe(r => {
                    if (r.success) {
                        this.files.splice(this.files.indexOf(this.selectedFile), 1);
                        delete this.selectedFile;
                    } else {
                        throw new Error(JSON.stringify(r.error));
                    }
                });
            }
        });
    }

    rename() {
        const renameDialogRef: MdDialogRef<RenameDialogComponent> = this.mdDialog.open(RenameDialogComponent);
        renameDialogRef.componentInstance.name = this.selectedFile.originalName;
        renameDialogRef.componentInstance.newName = this.selectedFile.originalName;
        renameDialogRef.afterClosed().subscribe(res => {
            if (res) {
                const newName = renameDialogRef.componentInstance.newName;
                this.healthService.renameFile(this.selectedFile.id, newName).subscribe(r => {
                    if (r.success) {
                        this.files.find(f => f.id === this.selectedFile.id).originalName = newName;
                    } else {
                        throw new Error(JSON.stringify(r.error));
                    }
                });
            }
        });
    }

    show() { }

    download() {
        window.open(this.selectedFile.path);
    }

    upload(files) {
        const observables = files
            .map(f => this.healthService.uploadFile(f))
            .map((request, i) => Observable.concat(Observable.of(i).delay(i * 1000), request));
        Observable.forkJoin(observables).subscribe(
            res => {
                res.map((r: any) => {
                    if (r.success) {
                        this.files.push(r.data);
                    } else {
                        throw new Error(JSON.stringify(r.error));
                    }
                });
            },
            err => { },
            () => {
                this.dropzoneService.change();
                this.mdSnackBar.open('Файлы загружены', '', {
                    duration: 1000,
                });
            }
        );
    }
}
