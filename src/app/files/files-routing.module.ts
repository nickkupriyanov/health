import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FilesComponent } from './files.component';
import { AuthGuardService } from '../shared/services/auth-guard.service';
import { ProfileGuardService } from '../shared/services/profile-guard.service';

const routes: Routes = [
    {
        path: 'files',
        component: FilesComponent,
        canActivate: [AuthGuardService, ProfileGuardService]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class FilesRoutingModule { }

export const routedComponents = [FilesComponent];
