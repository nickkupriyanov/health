import {
    Component,
    Input,
    Output,
    EventEmitter
} from '@angular/core';
import {
    subDays,
    addDays,
    addWeeks,
    subWeeks,
    addMonths,
    subMonths
} from 'date-fns';
import { User } from '../shared/models/user';
import { AuthService } from '../shared/services/auth.service';
import { MdTabNav } from '@angular/material';
import { Router } from '@angular/router';

@Component({
    selector: 'health-calendar-header',
    templateUrl: 'calendar-header.component.html',
    styleUrls: ['calendar-header.component.scss'],
    providers: [MdTabNav]
})
export class CalendarHeaderComponent {
    @Input() view: string;
    @Input() viewDate: Date;
    @Input() doctors: User[];
    @Output() viewDateChange: EventEmitter<Date> = new EventEmitter();
    @Output() doctorEvent = new EventEmitter();

    locale = 'ru';
    user = new User();

    constructor(
        private authService: AuthService,
        private router: Router,
    ) {
        this.authService.getUser().subscribe(res => {
            if (res.success) {
                this.user = res.data;
            } else {
                throw new Error(JSON.stringify(res.error));
            }
        });
    }

    increment(): void {
        const addFn: any = {
            day: addDays,
            week: addWeeks,
            month: addMonths
        }[this.view];
        this.viewDateChange.emit(addFn(this.viewDate, 1));
    }

    decrement(): void {
        const subFn: any = {
            day: subDays,
            week: subWeeks,
            month: subMonths
        }[this.view];
        this.viewDateChange.emit(subFn(this.viewDate, 1));
    }

    today(): void {
        this.viewDateChange.emit(new Date());
    }

    selectChange(event) {
        if (event.index === this.doctors.length) {
            this.router.navigate(['profile', 2]);
        } else {
            this.doctorEvent.emit(this.doctors[event.index]);
        }
    }
}
