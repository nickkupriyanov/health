import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IntermediateComponent } from './intermediate.component';

const routes: Routes = [
    { path: 'intermediate', component: IntermediateComponent },
    { path: 'intermediate/:id', component: IntermediateComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class IntermediateRoutingModule { }

export const routedComponents = [IntermediateComponent];
