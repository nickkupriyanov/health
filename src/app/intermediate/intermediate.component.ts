import {
    Component,
    OnInit,
    ElementRef,
    ViewChild
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { User } from '../shared/models/user';
import { HealthService } from '../shared/services/health.service';
import { AuthService } from '../shared/services/auth.service';
import { ValidationService } from '../shared/services/validation.service';
import { Router } from '@angular/router';
import { MdSelect } from '@angular/material';

@Component({
    selector: 'health-intermediate',
    templateUrl: 'intermediate.component.html',
    styleUrls: ['intermediate.component.scss']
})

export class IntermediateComponent implements OnInit {
    @ViewChild('image') image: ElementRef;

    user: User;
    allDoctors: User[] = [];
    src: string;
    form: FormGroup;

    constructor(
        private healthService: HealthService,
        private authService: AuthService,
        private formBuilder: FormBuilder,
        private router: Router,
        private validationService: ValidationService,
    ) {

        this.authService.getUser().subscribe(res => {
            if (res.success) {
                this.user = res.data;
                this.healthService.getAdmins().subscribe(r => {
                    if (r.success) {
                        this.allDoctors = r.data;
                        this.user.doctors = this.allDoctors.filter(f => this.user.doctors.map(d => d.id).includes(f.id));
                        this.buildForm(this.user);
                    } else {
                        throw new Error(JSON.stringify(r.error));
                    }
                });
                if (this.user.photo) {
                    this.src = this.user.photo;
                }
            } else {
                throw new Error(JSON.stringify(res.error));
            }
        });
    }

    ngOnInit() { }

    choosePhoto(): void {
        this.image.nativeElement.value = null;
        this.image.nativeElement.click();
    }

    onFileChange(file: File) {
        this.healthService.uploadProfilePhoto(file).subscribe(res => {
            if (res.success) {
                this.user.photo = res.data;
                this.healthService.editProfile(this.user).subscribe(r => {
                    if (r.success) {
                        this.user.photo = res.data;
                        if (this.user.photo.length > 0) {
                            this.src = this.user.photo;
                        }
                    } else {
                        throw new Error(JSON.stringify(res.error));
                    }
                });
            } else {
                throw new Error(JSON.stringify(res.error));
            }
        });
    }

    buildForm({ middleName = '', gender = '', phone = '', location = '', doctors = [] }) {
        this.form = this.formBuilder.group({
            'middleName': [middleName, ValidationService.emptyFieldValidator],
            'phone': [phone, ValidationService.emptyFieldValidator],
            'location': [location, ValidationService.emptyFieldValidator],
            'doctors': [doctors, [], this.validationService.doctorLengthValidator.bind(this.validationService)],
        });
    }

    submit() {
        this.healthService.editProfile(this.form.value).subscribe(res => {
            if (res.success) {
                this.router.navigate(['calendar']);
            } else {
                throw new Error(JSON.stringify(res.error));
            }
        });
    }

    signOut() {
        this.authService.signOut();
    }
}
