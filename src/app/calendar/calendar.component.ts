import { Component, OnInit } from '@angular/core';
import { HealthService } from '../shared/services/health.service';
import { AuthService } from '../shared/services/auth.service';
import { User } from '../shared/models/user';
import { MdDialog, MdDialogRef, MdSnackBar } from '@angular/material';
import { AppointmentDialogComponent } from '../shared/appointment-dialog/appointment-dialog.component';
import { CalendarMonthViewDay } from 'angular-calendar';

@Component({
    selector: 'health-calendar',
    templateUrl: 'calendar.component.html',
    styleUrls: ['calendar.component.scss'],
    providers: [MdDialog],
})

export class CalendarComponent implements OnInit {
    view = 'month';
    viewDate: Date = new Date();
    events: any[] = [];
    clickedDate: Date;
    locale = 'ru';
    weekStartsOn = 1;

    user: User;
    selectedDoctor: User;
    workTime;

    constructor(
        private healthService: HealthService,
        private authService: AuthService,
        private mdDialog: MdDialog,
        private mdSnackBar: MdSnackBar,
    ) {
        this.authService.getUser().subscribe(res => {
            if (res.success) {
                this.user = res.data;
                if (this.user.role === 0) {
                    this.getEvents(this.user.id);
                } else {
                    this.selectedDoctor = this.user.doctors[0];
                    this.getEvents(this.selectedDoctor.id);
                }
            } else {
                throw new Error(JSON.stringify(res.error));
            }
        });
        this.healthService.getWorkTime().subscribe(res => {
            if (res.success) {
                this.workTime = res.data.time;
            } else {
                throw new Error(JSON.stringify(res.error));
            }
        });
    }

    ngOnInit() { }

    getEvents(id: string) {
        this.healthService.getEvents().subscribe(res => {
            if (res.success) {
                this.events = res.data.map(e => {
                    e.start = new Date(e.start);
                    e.end = new Date(e.end);
                    return e;
                }).filter(e => e.doctorId === id);
                this.events.sort((a, b) => {
                    return a.start.getTime() - b.start.getTime();
                });
            } else {
                throw new Error(JSON.stringify(res.error));
            }
        });
    }

    dayClicked(day) {
        if (day.date < this.getToday()) {
            return;
        }
        const appointmentDialogRef: MdDialogRef<AppointmentDialogComponent> = this.mdDialog.open(AppointmentDialogComponent);
        appointmentDialogRef.componentInstance.date = day.date;
        appointmentDialogRef.componentInstance.doctor = this.selectedDoctor;
        appointmentDialogRef.componentInstance.role = this.user.role;
        appointmentDialogRef.componentInstance.times = this.getTimes(
            this.workTime,
            day.date,
            day.events.map(e => {
                return {
                    start: e.start,
                    end: e.end,
                };
            }));
        appointmentDialogRef.componentInstance.events = day.events;
        appointmentDialogRef.afterClosed().subscribe(res => {
            if (res.action === 'add') {
                const event = {
                    title: `${this.user.firstName} ${this.user.lastName}`,
                    start: res.value.start,
                    end: res.value.end,
                    color: {
                        primary: '#20b2aa',
                        secondary: '#20b2aa'
                    },
                    doctorId: this.selectedDoctor.id,
                };
                this.healthService.addEvent(event).subscribe(r => {
                    if (r.success) {
                        this.getEvents(this.selectedDoctor.id);
                        this.mdSnackBar.open('Запись добавлена', '', {
                            duration: 1500,
                        });
                    } else {
                        throw new Error(JSON.stringify(res.error));
                    }
                });
            }
        });
    }

    getTimes(time: { start: string, end: string }, date: Date, busyTime: any[]) {
        const start = parseInt(time.start, 10);
        const end = parseInt(time.end, 10);
        const dateAt = h => new Date(date.getFullYear(), date.getMonth(), date.getDate(), h);
        const times = Array.from(Array(end - start).keys())
            .map(x => x + start)
            .map(x => {
                return {
                    start: dateAt(x),
                    end: dateAt(x + 1)
                };
            });
        return times.map(t => {
            if (busyTime.map(b => JSON.stringify(b)).includes(JSON.stringify(t))) {
                return {
                    busy: true,
                    time: t,
                };
            } else {
                return {
                    busy: false,
                    time: t,
                };
            }
        });
    }

    selectToday(event) {
        console.log(event);
    }

    doctorSelect(event) {
        this.selectedDoctor = event;
        this.getEvents(this.selectedDoctor.id);
    }

    getToday() {
        return new Date(new Date().setHours(0, 0, 0, 0));
    }

    beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }) {
        body.forEach(day => {
            if (
                day.date.getDay() === 0 ||
                day.date.getDay() === 6 ||
                day.date < this.getToday()
            ) {
                day.cssClass = 'cal-disabled';
            }
        });
    }
}
