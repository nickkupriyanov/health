import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalendarComponent } from './calendar.component';
import { AuthGuardService } from '../shared/services/auth-guard.service';
import { ProfileGuardService } from '../shared/services/profile-guard.service';

const routes: Routes = [
    {
        path: 'calendar',
        component: CalendarComponent,
        canActivate: [AuthGuardService, ProfileGuardService]
    },
    {
        path: 'calendar/:id',
        component: CalendarComponent,
        canActivate: [AuthGuardService, ProfileGuardService]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CalendarRoutingModule { }

export const routedComponents = [CalendarComponent];
