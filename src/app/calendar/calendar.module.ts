import { NgModule } from '@angular/core';
import { CalendarModule as NgxCalendarModule } from 'angular-calendar';
import { CalendarComponent } from './calendar.component';
import { CalendarRoutingModule } from './calendar-routing.module';
import { SharedModule } from '../shared/shared.module';
import { CalendarHeaderComponent } from '../calendar-header/calendar-header.component';
import { AppointmentDialogComponent } from '../shared/appointment-dialog/appointment-dialog.component';

@NgModule({
    imports: [
        CalendarRoutingModule,
        SharedModule,
        NgxCalendarModule.forRoot(),
    ],
    exports: [],
    declarations: [
        CalendarComponent,
        CalendarHeaderComponent,
        AppointmentDialogComponent,
    ],
    providers: [],
    entryComponents: [
        AppointmentDialogComponent,
    ]
})
export class CalendarModule { }
