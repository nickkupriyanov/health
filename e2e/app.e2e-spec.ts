import { Healt2.0Page } from './app.po';

describe('healt2.0 App', () => {
  let page: Healt2.0Page;

  beforeEach(() => {
    page = new Healt2.0Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
